<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h2>MOBIL</h2>
            </div>
        </div>
    </div>
    <div class="row mb-50 mt-50">  
        <div class="row">
        <?php
            
            $sql = mysqli_query ($koneksi, "select detail_mobil.jenis_mobil, detail_mobil.spesifikasi, detail_mobil.id_detail_mobil, detail_mobil.img, dp.harga from detail_mobil, dp where detail_mobil.tipe = dp.tipe group by jenis_mobil;");
            while ($data = mysqli_fetch_array ($sql)){
        ?>
            <div class="col-md-3">
                <div class="card mb-4 shadow-sm">
                    <div class="gambar">
                        <img src="img/mobil/<?php echo $data['img'];?>" class="img img-responsive" alt="<?php echo $data['img'];?>">
                        <div class="card-body">
                            <p class="card-text"><?php echo ucwords($data['jenis_mobil']);?></p>
                            <p class="text-right">Mulai Dari <strong class="text-primary"><?php echo "Rp ".number_format($data['harga']);?></strong></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="?tampil=mobil_detail&id=<?php echo $data['id_detail_mobil'];?>" class="btn btn-md btn-outline-secondary">
                                        <i class="fa fa-shopping-basket"></i> Shop
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="img/spesifikasi/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        <?php
        }
        ?>
        </div>
        <!-- album 1 -->
    </div>
</section>