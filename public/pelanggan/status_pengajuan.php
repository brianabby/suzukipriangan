
<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="container">
        <div class="col-md-12">
            <hr>
        <div class="row">
        <div class="col-md-9">
            <p>
                <Strong>Data Pengajuan</Strong>
                <br>
                <small>Berikut data pengajuan Anda: Kredit, Cash dan Trade In</small>
            </p>
            <hr>
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">Pengajuan Perorangan</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">Pengajuan Perusahaan</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane container active" id="home">
                            <br>
                            <p class="text-dark"><b>Data Pengajuan Kredit</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql = mysqli_query ($koneksi, "SELECT * FROM kredit_perorangan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sql) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Kredit!</td></tr>';
                    }else{
                        while ($data = mysqli_fetch_array ($sql)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data['tipe']; ?></td>
                        <td><?php echo $data['warna']; ?></td>
                        <td><?php echo $data['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
            <br>
            <p class="text-dark"><b>Data Pengajuan Cash</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql_3 = mysqli_query ($koneksi, "SELECT * FROM cash_perorangan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sql_3) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Cash!</td></tr>';
                    }else{
                        while ($data_3 = mysqli_fetch_array ($sql_3)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data_3['tipe']; ?></td>
                        <td><?php echo $data_3['warna']; ?></td>
                        <td><?php echo $data_3['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                            <br>
            <p class="text-dark"><b>Data Pengajuan Trade In</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql_4 = mysqli_query ($koneksi, "SELECT * FROM tradein_perorangan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sql_4) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Trade In!</td></tr>';
                    }else{
                        while ($data_4 = mysqli_fetch_array ($sql_4)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data_4['tipe']; ?></td>
                        <td><?php echo $data_4['warna']; ?></td>
                        <td><?php echo $data_4['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                        </div>

                                    <div class="tab-pane container fade" id="menu1">
                                        <br>
                                        <p class="text-dark"><b>Data Pengajuan Kredit</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql_2 = mysqli_query ($koneksi, "SELECT * FROM kredit_perusahaan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sq_2l) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Kredit!</td></tr>';
                    }else{
                        while ($data_2 = mysqli_fetch_array ($sql_2)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data_2['tipe']; ?></td>
                        <td><?php echo $data_2['warna']; ?></td>
                        <td><?php echo $data_2['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                                        <br>
            <p class="text-dark"><b>Data Pengajuan Cash</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql_5 = mysqli_query ($koneksi, "SELECT * FROM cash_perusahaan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sql_5) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Cash!</td></tr>';
                    }else{
                        while ($data_5 = mysqli_fetch_array ($sql_5)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data_5['tipe']; ?></td>
                        <td><?php echo $data_5['warna']; ?></td>
                        <td><?php echo $data_5['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                            <br>
            <p class="text-dark"><b>Data Pengajuan Trade In</b></p>
            <table class="table .table-striped text-center">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Tipe Mobil</th>
                        <th scope="col">Warna</th>
                        <th scope="col">Status</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 0;
                    $sql_6 = mysqli_query ($koneksi, "SELECT * FROM tradein_perusahaan WHERE no_hp='$konsumen[phone_number]'");
                    if (mysqli_num_rows($sql_6) == 0){    
                        echo '<tr><td colspan="6">Belum ada data pengajuan Trade In!</td></tr>';
                    }else{
                        while ($data_6 = mysqli_fetch_array ($sql_6)){
                            $no++;
                    ?>
                    <tr>
                        <th scope="row"><?php echo $no; ?>.</th>
                        <td><?php echo $data_6['tipe']; ?></td>
                        <td><?php echo $data_6['warna']; ?></td>
                        <td><?php echo $data_6['status']; ?></td>
                        <td>Lihat</td>
                    </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
                                    </div>
                                    <div class="tab-pane container fade" id="menu2">
                                        <br><br>
                                        <h3 class="text-center">Belum ada ulasan</h3>
                                        <br>
                                        <p class="text-center">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
            
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                        <h6 class="text-center"><?php echo $konsumen['nama_konsumen']; ?></h6>
                    </div>
                    <p class="text-left">
                        <a href="?tampil=profil"><small><img src="http://suzukipriangan.com/img/core-img/sm_profil.png" alt="" class="col-md-3"> Profil</small></a>
                        <br>
                        <a href="?tampil=status_pengajuan"><small><img src="http://suzukipriangan.com/img/core-img/icon-transaksi.png" alt="" class="col-md-3">Status Pengajuan</small></a>
                        <br>
                        <a href="?tampil=transaksi"><small><img src="http://suzukipriangan.com/img/core-img/credit-card.png" alt="" class="col-md-3">Transaksi Pembelian</small></a>
                        <br>
                        <a href="?tampil=pesanan"><small><img src="http://suzukipriangan.com/img/core-img/Online-Booking-Icon.jpg" alt="" class="col-md-3">Pesanan Saya</small></a>
                        <br>
                        <a href="?tampil=alamat"><small><img src="http://suzukipriangan.com/img/core-img/address.png" alt="" class="col-md-3">Alamat Saya</small></a>
                        <br>
                        <a href="password"><small><img src="http://suzukipriangan.com/img/core-img/password.png" alt="" class="col-md-3">Atur Password</small></a>
                    </p>
                
            </div>
        </div>
        </div>
        </div>
    </div>
</section>