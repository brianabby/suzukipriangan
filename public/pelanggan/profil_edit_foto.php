<?php

include ("lib/koneksi.php");

date_default_timezone_set('Asia/Jakarta');

//file 1
$temp      = $_FILES['foto']['tmp_name'];
$name      = $_FILES['foto']['name'];
$nama_baru = date("dmY_h:i:sa-").$name;
$size      = $_FILES['foto']['size'];
$type      = $_FILES['foto']['type'];
$folder    = "img/foto_pelanggan/";

if($type == "image/jpeg" || $type == "image/jpg" || $type == "image/gif" || $type == "image/png")
{
    $delete_file = "img/foto_pelanggan/$konsumen[foto]";
    $files = glob($delete_file); //
    foreach($files as $file){
        if(is_file($file))
            unlink($file);
    }
    
    // upload 
    move_uploaded_file($temp, $folder.$nama_baru);
    
    mysqli_query ($koneksi, "UPDATE konsumen SET
    foto = '$nama_baru' WHERE id_konsumen=$konsumen[id_konsumen]") or die ("Gagal melakukan pesanan");
    
    echo "<meta http-equiv='refresh' content='0; url=?tampil=profil'>";
}
else{
    echo "Harap upload gambar dengan Format jpg/jpeggif/png dan Ukuran Max 2MB";
}