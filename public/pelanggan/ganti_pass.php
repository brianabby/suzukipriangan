<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="container">
        <div class="col-md-12">
            <hr>
        <div class="row">
        <div class="col-md-9">
            
                    <p>
                        <Strong>Profil Saya</Strong>
                        <br>
                        <small>Gunakan kombinasi huruf dan angka untuk kekuatan password Anda</small>
                    </p>
                    <hr>
                
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                        <h6 class="text-center"><?php echo $konsumen['nama_konsumen']; ?></h6>
                    </div>
                    <p class="text-left">
                        <a href="?tampil=profil"><small><img src="http://suzukipriangan.com/img/core-img/sm_profil.png" alt="" class="col-md-3"> Profil</small></a>
                        <br>
                        <a href="?tampil=status_pengajuan"><small><img src="http://suzukipriangan.com/img/core-img/icon-transaksi.png" alt="" class="col-md-3">Status Pengajuan</small></a>
                        <br>
                        <a href="?tampil=transaksi"><small><img src="http://suzukipriangan.com/img/core-img/credit-card.png" alt="" class="col-md-3">Transaksi Pembelian</small></a>
                        <br>
                        <a href="?tampil=pesanan"><small><img src="http://suzukipriangan.com/img/core-img/Online-Booking-Icon.jpg" alt="" class="col-md-3">Pesanan Saya</small></a>
                        <br>
                        <a href="?tampil=alamat"><small><img src="http://suzukipriangan.com/img/core-img/address.png" alt="" class="col-md-3">Alamat Saya</small></a>
                        <br>
                        <a href="?tampil=ganti_pass"><small><img src="http://suzukipriangan.com/img/core-img/password.png" alt="" class="col-md-3">Atur Password</small></a>
                    </p>
                
            </div>
        </div>
        </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-sm" id="UbahEmail" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Ganti Email</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="?tampil=profil_edit_foto" enctype="multipart/form-data">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-right">Email Sekarang</td>
                                                            <td class="text-left"><div class="form-group has-feedback">
                                                            <input type="text" class="form-control form-control-sm" value="<?php echo $konsumen['email']; ?>">
                                                            </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">Password Baru</td>
                                                <td class="text-left"> <div class="form-group has-feedback">
                                                    <input class="form-control form-control-sm" id="password" name="password" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Minimal 6 Karakter' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="Password" required>
                                                    <span class="form-control-feedback"></span>
                                                    <span class="text-warning"></span>
                                                </div>
                                                            </td>
                                                    </tr>
                                                        <tr>
                                                        <td class="text-right">Ulangi Password baru</td>
                                                <td class="text-left"><div class="form-group has-feedback">
                                                    <input class="form-control form-control-sm" id="password_two" name="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Masukkan Password Yang Sama' : '');" placeholder="Verify Password" required>
                                                    <span class="form-control-feedback"></span>
                                                    <span class="text-warning"></span>
                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right"></td>
                                                <td class="text-left"><button type="submit" class="btn btn-outline-primary btn-sm">Simpan</button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
</section>