
<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="container">
        <div class="col-md-12">
            <hr>
        <div class="row">
        <div class="col-md-9">
            
                    <p>
                        <Strong>Data Transaksi</Strong>
                        <br>
                        <small>Berikut data transaksi Anda: Pembelian Mobil, Spareparts dan Aksesoris</small>
                    </p>
                    <hr>
            <br>
                            <p class="text-dark"><b>Data Transaksi</b></p>
            <table class="table .table-striped text-center">
                <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nomor Transaksi</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Keterangan</th>
    </tr>
  </thead>
  <tbody>
      <?php
            $no = 0;
            $sql = mysqli_query ($koneksi, "SELECT * FROM transaksi WHERE no_hp='$konsumen[phone_number]'");
            if (mysqli_num_rows($sql) == 0){    
                                echo '<tr><td colspan="6">Belum ada data transaksi!</td></tr>';
            }else{
            while ($data = mysqli_fetch_array ($sql)){
            $no++;
        ?>
    <tr>
      <th scope="row"><?php echo $no; ?></th>
      <td><?php echo $data['no_transaksi']; ?></td>
      <td><?php echo $data['tanggal']; ?></td>
      <td><?php echo $data['keterangan']; ?></td>
    </tr>
      <?php
            }
            }
      ?>
  </tbody>
            </table>
                
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                        <h6 class="text-center"><?php echo $konsumen['nama_konsumen']; ?></h6>
                    </div>
                    <p class="text-left">
                        <a href="?tampil=profil"><small><img src="http://suzukipriangan.com/img/core-img/sm_profil.png" alt="" class="col-md-3"> Profil</small></a>
                        <br>
                        <a href="?tampil=status_pengajuan"><small><img src="http://suzukipriangan.com/img/core-img/icon-transaksi.png" alt="" class="col-md-3">Status Pengajuan</small></a>
                        <br>
                        <a href="?tampil=transaksi"><small><img src="http://suzukipriangan.com/img/core-img/credit-card.png" alt="" class="col-md-3">Transaksi Pembelian</small></a>
                        <br>
                        <a href="?tampil=pesanan"><small><img src="http://suzukipriangan.com/img/core-img/Online-Booking-Icon.jpg" alt="" class="col-md-3">Pesanan Saya</small></a>
                        <br>
                        <a href="?tampil=alamat"><small><img src="http://suzukipriangan.com/img/core-img/address.png" alt="" class="col-md-3">Alamat Saya</small></a>
                        <br>
                        <a href="password"><small><img src="http://suzukipriangan.com/img/core-img/password.png" alt="" class="col-md-3">Atur Password</small></a>
                    </p>
                
            </div>
        </div>
        </div>
        </div>
    </div>
</section>