<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="container">
        <div class="col-md-12">
            <hr>
        <div class="row">
        <div class="col-md-9">
            
                    <p>
                        <Strong>Profil Saya</Strong>
                        <br>
                        <small>Kelola informasi profil untuk mempercepat proses transaksi Anda di Suzuki Priangan</small>
                    </p>
                    <hr>
                <div class="row">
            <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                        <div class="profil text-center">
                            <?php
                                $cek = mysqli_query ($koneksi, "SELECT foto FROM konsumen where id_konsumen='$konsumen[id_konsumen]'");
                                $hasil_cek = mysqli_fetch_array ($cek);
                                if ($hasil_cek['foto']<= 0){
                            ?>
                            <img src="../img/profil.png" alt="" class="rounded-circle">
                            <?php
                                }
                                elseif ($hasil_cek['foto']>=1){
                            ?>
                            <img src="../img/foto_pelanggan/<?php echo $hasil_cek['foto']; ?>" alt="" class="rounded-circle">
                            <?php
                                }
                            ?>
                            <br><br>
                            <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#PilihFoto">Pilih Foto</button>
                            <div class="modal fade bd-example-modal-sm" id="PilihFoto" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Pilih Foto</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="?tampil=profil_edit_foto" enctype="multipart/form-data">
                                            <p><input type="file" id="foto" onchange="return fileValidation()" class="form-control form-control-sm" name="foto" required></p>
                                            <div id="imagePreview"></div>
                                            <br>
                                            <button type="submit" class="btn btn-outline-primary btn-sm">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-md-7">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td class="text-right">Email</td>
                            <td class="text-left"><?php echo $konsumen['email']; ?> <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#UbahEmail">Ubah</button></td>
                            
                        </tr>
                        <tr>
                            <td class="text-right">Nomor Telepon</td>
                            <td class="text-left"><?php echo $konsumen['phone_number']; ?> <a href="#"  class="text-primary"><small style="text-decoration: underline;">Ubah</small></a></td>
                        </tr>
                        <form method="post" action="?tampil=profil_edit_proses" enctype="multipart/form-data">
                        <tr>
                            <td class="text-right">No. KTP</td>
                            <td class="text-left"><input type="text" name="no_ktp" class="form-control form-control-sm" value="<?php echo $konsumen['no_ktp']; ?>"></td>
                        </tr>
                        <?php
                            if ($konsumen['foto_ktp']<= 0){
                                
                            }
                            elseif ($konsumen['foto_ktp']>= 1){
                        ?>
                        <tr>
                            <td></td>
                            <td class="text-center"><img src="../img/foto_ktp_pelanggan/<?php echo $konsumen['foto_ktp']; ?>" class="col-md-9" alt=""></td>
                        </tr>
                        <?php
                            }
                        ?>
                        <tr>
                            <td class="text-right">Pilih Foto KTP</td>
                            <td class="text-left"><input type="file" name="foto_ktp" id="foto_ktp" onchange="return fileValidation()" class="form-control form-control-sm" value="Upload KTP"></td>
                        </tr>
                        <tr>
                            <td class="text-right"></td>
                            <td class="text-left"><div id="imagePreview_2"></div></td>
                        </tr>
                        <tr>
                            <td class="text-right">Nama</td>
                            <td class="text-left"><input type="text" name="nama" class="form-control form-control-sm" value="<?php echo $konsumen['nama_konsumen']; ?>"></td>
                        </tr>
                        <tr>
                            <td class="text-right">Jenis Kelamin</td>
                            <td class="text-left"><select id="inputState" name="jenis_kelamin" class="form-control form-control-sm">
                                <option selected><?php echo $konsumen['jenis_kelamin']; ?></option>
                                <option>Laki-laki</option>
                                <option>Perempuan</option>
                                </select></td>
                        </tr>
                        <tr>
                            <td class="text-right">Tanggal lahir</td>
                            <td class="text-left"><input type="date" name="tanggal_lahir" class="form-control form-control-sm" value="<?php echo $konsumen['tgl_lahir']; ?>"></td>
                        </tr>
                        <tr>
                            <td class="text-right"></td>
                            <td class="text-left"><button type="submit" class="btn btn-outline-primary btn-sm">Simpan</button></td>
                        </tr>
                        </form>
                    </tbody>
                </table>
            </div>
                    </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                        <h6 class="text-center"><?php echo $konsumen['nama_konsumen']; ?></h6>
                    </div>
                    <p class="text-left">
                        <a href="?tampil=profil"><small><img src="http://suzukipriangan.com/img/core-img/sm_profil.png" alt="" class="col-md-3"> Profil</small></a>
                        <br>
                        <a href="?tampil=status_pengajuan"><small><img src="http://suzukipriangan.com/img/core-img/icon-transaksi.png" alt="" class="col-md-3">Status Pengajuan</small></a>
                        <br>
                        <a href="?tampil=transaksi"><small><img src="http://suzukipriangan.com/img/core-img/credit-card.png" alt="" class="col-md-3">Transaksi Pembelian</small></a>
                        <br>
                        <a href="?tampil=pesanan"><small><img src="http://suzukipriangan.com/img/core-img/Online-Booking-Icon.jpg" alt="" class="col-md-3">Pesanan Saya</small></a>
                        <br>
                        <a href="?tampil=alamat"><small><img src="http://suzukipriangan.com/img/core-img/address.png" alt="" class="col-md-3">Alamat Saya</small></a>
                        <br>
                        <a href="?tampil=ganti_pass"><small><img src="http://suzukipriangan.com/img/core-img/password.png" alt="" class="col-md-3">Atur Password</small></a>
                    </p>
                
            </div>
        </div>
        </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-sm" id="UbahEmail" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Ganti Email</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="post" action="?tampil=profil_edit_foto" enctype="multipart/form-data">
                                                <table class="table table-borderless">
                                                    <tbody>
                                                        <tr>
                                                            <td class="text-right">Email Sekarang</td>
                                                            <td class="text-left"><div class="form-group has-feedback">
                                                            <input type="text" class="form-control form-control-sm" value="<?php echo $konsumen['email']; ?>">
                                                            </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right"></td>
                                                <td class="text-left"> <div class="form-group has-feedback">
                                                    <input class="form-control form-control-sm" id="password" name="password" type="text" value="<?php echo (md5($konsumen['password'])); ?>" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Minimal 6 Karakter' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="Password" required>
                                                    <span class="form-control-feedback"></span>
                                                    <span class="text-warning"></span>
                                                </div>
                                                            </td>
                                                    </tr>
                                                        <tr>
                                                        <td class="text-right">Password Anda</td>
                                                <td class="text-left"><div class="form-group has-feedback">
                                                    <input class="form-control form-control-sm" id="password_two" name="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Masukkan Password Yang Sama' : '');" placeholder="Verify Password" required>
                                                    <span class="form-control-feedback"></span>
                                                    <span class="text-warning"></span>
                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right"></td>
                                                <td class="text-left"><button type="submit" class="btn btn-outline-primary btn-sm">Simpan</button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
</section>
<script type="text/javascript">
    function fileValidation(){
    var fileInput = document.getElementById('foto');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>
<script type="text/javascript">
    function fileValidation(){
    var fileInput = document.getElementById('foto_ktp');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview_2').innerHTML = '<img src="'+e.target.result+'"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
}
</script>