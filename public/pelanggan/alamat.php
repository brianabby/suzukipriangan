
<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="container">
        <div class="col-md-12">
            <hr>
            <div class="row">
                <div class="col-md-9">
                    <p>
                        <Strong>Alamat</Strong>
                        <br>
                        <small>Gunakan alamat yang lengkap dan valid untuk mempercepat proses pesanan Anda</small>
                    </p>
                    <hr>
                    <br>
                    <p class="text-dark"><b>Data Alamat</b><button type="button" data-toggle="modal" class="btn btn-outline-primary btn-sm float-right" data-target="#ModalTambahAlamat">Tambah Alamat</button></p>
                    <!-- Modal -->
                    <div class="modal fade" id="ModalTambahAlamat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Tambah Alamat</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="?tampil=alamat_tambah_proses" method="POST">
                                            <div class="col-sm">
                                                <div class="box box-info">
                                                    <div class="box-body">
                                                        <input id="id_konsumen" type="text" name="id_konsumen" class="form-control" placeholder="id" value="<?php echo $konsumen['id_konsumen']; ?>" hidden />
                                                        <div class="form-group">
                                                            <label>Nama Penerima</label>
                                                                <input id="nama" type="text" name="nama_penerima" class="form-control form-control-sm" placeholder="Nama Penerima" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="box box-info">
                                                     <div class="form-group">
                                                            <label>No Telepon / HP</label>
                                                            <input id="telepon" type="text" name="no_hp" class="form-control form-control-sm" placeholder="No Telepon / HP" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label>Kabupaten</label>
                                                            <select class="form-control form-control-sm" name="kabupaten" id="kabupaten" required>
                                                                <option>Pilih Kabupaten</option>
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct kabupaten FROM kabupaten order by kabupaten");
                                                            while($data_kab=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_kab["kabupaten"] ?>"><?php echo $data_kab["kabupaten"] ?></option>
    
                                                            <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Kecamatan</label>
                                                                <select class="form-control form-control-sm" name="kecamatan" id="kecamatan" required>
                                                                </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat Lengkap</label>
                                                            <textarea rows="3" name="alamat_penerima" class="form-control form-control-sm" placeholder="Alamat Lengkap" required></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="box box-info">
                                                        <div class="form-group">
                                                            <label>Kode POS</label>
                                                            <input id="kode_pos" type="text" name="kode_pos" class="form-control form-control-sm" placeholder="Kode POS" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                        <div class="footer">                    
                                                            <div class="container">
                                                                <div class="row">
                                                                    <div class="col">
                                                                    </div>
                                                                    <div class="col-6">
                                                                    </div>
                                        
                                                                </div>
                                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                                </div>
                                    </form>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <table class="table .table-striped text-center">
                        <thead>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Nama Penerima</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">No. Hp</th>
                                <th scope="col">Kode Pos</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 0;
                            $sql = mysqli_query ($koneksi, "SELECT * FROM alamat WHERE id_konsumen='$konsumen[id_konsumen]'");
                            if (mysqli_num_rows($sql) == 0){    
                                echo '<tr><td class="text-center" colspan="6">Belum ada data alamat!</td></tr>';
                            }else{
                                while ($data = mysqli_fetch_array ($sql)){
                                    $no++;
                            ?>
                            <tr>
                                <th scope="row"><?php echo $no; ?></th>
                                <td><?php echo $data['nama_penerima']; ?></td>
                                <td><?php echo $data['alamat_penerima']; ?></td>
                                <td><?php echo $data['no_hp']; ?></td>
                                <td><?php echo $data['kode_pos']; ?></td>
                                <td class="align-middle"><a href="?tampil=keranjang&id=<?php echo $data['id_aksesoris'];?>" title="Hapus"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                
                </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                        <h6 class="text-center"><?php echo $konsumen['nama_konsumen']; ?></h6>
                    </div>
                    <p class="text-left">
                        <a href="?tampil=profil"><small><img src="http://suzukipriangan.com/img/core-img/sm_profil.png" alt="" class="col-md-3"> Profil</small></a>
                        <br>
                        <a href="?tampil=status_pengajuan"><small><img src="http://suzukipriangan.com/img/core-img/icon-transaksi.png" alt="" class="col-md-3">Status Pengajuan</small></a>
                        <br>
                        <a href="?tampil=transaksi"><small><img src="http://suzukipriangan.com/img/core-img/credit-card.png" alt="" class="col-md-3">Transaksi Pembelian</small></a>
                        <br>
                        <a href="?tampil=pesanan"><small><img src="http://suzukipriangan.com/img/core-img/Online-Booking-Icon.jpg" alt="" class="col-md-3">Pesanan Saya</small></a>
                        <br>
                        <a href="?tampil=alamat"><small><img src="http://suzukipriangan.com/img/core-img/address.png" alt="" class="col-md-3">Alamat Saya</small></a>
                        <br>
                        <a href="?tampil=password"><small><img src="http://suzukipriangan.com/img/core-img/password.png" alt="" class="col-md-3">Atur Password</small></a>
                    </p>
                
            </div>
        </div>
        </div>
        </div>
    </div>
</section>
<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        
        // variabel dari nilai combo box unit
        var id_provinsi = $("#provinsi").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kabupaten.php",
            data: "provinsi_id="+id_provinsi,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kabupaten');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kabupaten").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>