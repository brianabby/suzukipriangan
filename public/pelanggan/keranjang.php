<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="row md-30 mt-50">
        <div class="col-md-8 offset-sm-2">
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h4>Keranjang Belanja</h4>
                <br>
            </div>
            <div class="table-responsive-xl text-center">
                <table class="table table-fixed">
                    <thead>
                        <tr>
                            <th colspan="2" scope="col" width="130px">Produk</th>
                            <th scope="col" width="120px">Harga Satuan</th>
                            <th scope="col">Banyak</th>
                            <th scope="col" width="120px">Total Harga</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 0;
                        $sql = mysqli_query ($koneksi, "SELECT * FROM keranjang as ps JOIN aksesoris as cs ON ps.id_aksesoris=cs.id_aksesoris where ps.id_konsumen='$konsumen[id_konsumen]'");
                        
                        if (mysqli_num_rows($sql) == 0){    
                            echo '<tr><td colspan="6">Keranjang masih kosong!</td></tr>';
                        }else{
                            while ($data = mysqli_fetch_array ($sql)){
                                $no++;
                        ?>
                        <tr>
                            <td class="align-middle"><?php echo $data['nama']; ?></td>
                            <td class="align-middle"><img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" class="col-md-8"></td>
                            <td class="align-middle"><?php echo "Rp ".number_format($data['harga']); ?></td>
                            <td class="align-middle"><?php echo $data['banyak']; ?></td>
                            <td class="align-middle"><?php echo "Rp ".number_format($data['total']); ?></td>
                            <td class="align-middle"><a href="?tampil=keranjang_hapus&id=<?php echo $data['id_keranjang'];?>" onclick="return confirm('Apakah Anda yakin ingin menghapus produk ini dari keranjang?')" title="Hapus"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                        </tr>
                        <?php
                                
                            }
                        }
                            $total = mysqli_query ($koneksi, "select sum(total) as jumlah_total from keranjang where id_konsumen='$konsumen[id_konsumen]'");
                            $jumlah_total = mysqli_fetch_array ($total);
                        ?>
                        <tr>
                            <th colspan="2" class="text-left align-middle">Subtotal Produk : </th>
                            <th></th>
                            <th></th>
                            <th class="align-middle"><?php echo "Rp ".number_format($jumlah_total['jumlah_total']); ?></th>
                        <?php
                            $sql_no = mysqli_query ($koneksi, "SELECT * FROM keranjang as ps JOIN aksesoris as cs ON ps.id_aksesoris=cs.id_aksesoris where ps.id_konsumen='$konsumen[id_konsumen]'");
                        
                            if (mysqli_num_rows($sql_no) == 0){ 
                        ?>
                            <th><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#DataKosong">Check Out</button></th>
                            <div class="modal fade bd-example-modal-sm" id="DataKosong" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Keranjang  Anda masih kosong!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                            elseif (mysqli_num_rows($sql_no) >= 1) {
                        ?>
                            <th><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#PilihAlamat">Check Out</button></th>
                        <?php
                            }
                        ?>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal fade" id="PilihAlamat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    
                    <div class="modal-content">
                        
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form class="form-group" action="?tampil=pemesanan_proses" method="post">
                        <div class="modal-body">
                            <div class="text-center">
                            <table class="table table-fixed">
                                <thead>
                                    <tr>
                                        <th scope="col" width="130px">Produk</th>
                                        <th scope="col" width="120px">Harga Satuan</th>
                                        <th scope="col">Banyak</th>
                                        <th scope="col" width="120px">Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    $sql = mysqli_query ($koneksi, "SELECT * FROM keranjang as ps JOIN aksesoris as cs ON ps.id_aksesoris=cs.id_aksesoris where ps.id_konsumen='$konsumen[id_konsumen]'");
                        
                                    if (mysqli_num_rows($sql) == 0){    
                                        echo '<tr><td colspan="6">Keranjang masih kosong!</td></tr>';
                                    }else{
                                        while ($data = mysqli_fetch_array ($sql)){
                                            $no++;
                                    ?>
                                    <tr>
                                        <td class="align-middle"><?php echo $data['nama']; ?></td>
                                        <td class="align-middle"><?php echo "Rp ".number_format($data['harga']); ?></td>
                                        <td class="align-middle"><?php echo $data['banyak']; ?></td>
                                        <td class="align-middle"><?php echo "Rp ".number_format($data['total']); ?></td>
                                    </tr>
                                    <?php
                                
                                        }
                                    }
                                    $total = mysqli_query ($koneksi, "select sum(total) as jumlah_total from keranjang where id_konsumen='$konsumen[id_konsumen]'");
                                    $jumlah_total = mysqli_fetch_array ($total);
                                    ?>
                                    <tr>
                                        <th colspan="2" class="text-left align-middle">Subtotal Produk : </th>
                                        <th></th>
                                        <th class="align-middle"><?php echo "Rp ".number_format($jumlah_total['jumlah_total']); ?></th>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <label>Pilih Alamat</label>
                            <div class="form-row">
                                <div class="form-group col-md-8">
                                <select class="form-control form-control-sm" name="alamat" id="exampleFormControlSelect1" required>
                                    <?php
                                    $no = 0;
                                    $sql_3 = mysqli_query ($koneksi, "SELECT * FROM alamat WHERE id_konsumen='$konsumen[id_konsumen]'");
                                    if (mysqli_num_rows($sql_3) == 0){    
                                    ?>
                                    <option></option>
                                    <?php
                                    }else{
                                        while ($alamat = mysqli_fetch_array ($sql_3)){
                                    ?>
                                    <option><?php echo "$alamat[alamat_penerima], $alamat[nama_kecamatan], $alamat[kabupaten]"; ?></option>
                                    <?php
                                        }
                                    }
                                    ?>
                                </select>
                                </div>
                                <div class="form-group col-md-4 text-center">
                                <a href="?tampil=alamat"><small style="color:blue">Kelola Alamat <i class="fa fa-arrow-right" style="color:blue"></i></small></a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary btn-sm">Check Out</button>
                        </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>