<?php

    include ("lib/koneksi.php");

?>  
<div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h5>Tambah Alamat</h5>
                <br>
            </div>
        </div>
    </div>
<section class="mb-5">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-box">
                            <section class="content">
                                <form action="?tampil=alamat_tambah_proses" method="POST">
                                <div class="row">
                                    <div class="col-sm">
                                                <div class="box box-info">
                                                    <div class="box-body">
                                                        <input id="id_konsumen" type="text" name="id_konsumen" class="form-control" placeholder="id" value="<?php echo $konsumen['id_konsumen']; ?>" hidden />
                                                        <div class="form-group">
                                                        <label>Nama Penerima</label>
                                                            <input id="nama" type="text" name="nama_penerima" class="form-control" placeholder="Nama Penerima" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="box box-info">
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label>Kabupaten</label>
                                                            <select class="form-control" name="kabupaten" id="kabupaten">
                                                                <option>Pilih Kabupaten</option>
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct kabupaten FROM kabupaten order by kabupaten");
                                                            while($data_kab=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_kab["kabupaten"] ?>"><?php echo $data_kab["kabupaten"] ?></option>
    
                                                            <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                                <div class="form-group">
                                                                <label>Alamat</label>
                                                                    <textarea rows="3" name="alamat_penerima" class="form-control" placeholder="Alamat"></textarea>
                                                                </div>
                                                               
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    
                     </div> 
                     
                     <div class="col-md-6">
                        <div class="form-box" id="login-box">
                                <div class="box box-info">
                                                    <div class="form-head">
                                                    </div>
                                <div class="box-body">
                                    <div class="form-group">
                                            <label>No Telepon / HP</label>
                                            <input id="telepon" type="text" name="no_hp" class="form-control" placeholder="No Telepon / HP" required textbox/>
                                            <span class="form-control-feedback"></span>
                                            <span class="text-warning"></span>
                                    </div>
                                    <div class="form-group">
                                                            <label>Kecamatan</label>
                                                                <select class="form-control" name="kecamatan" id="kecamatan">
                                                                </select>
                                                        </div>
                                    <div class="form-group">
                                            <label>Kode POS</label>
                                            <input id="kode_pos" type="text" name="kode_pos" class="form-control" placeholder="Kode POS" required textbox/>
                                            <span class="form-control-feedback"></span>
                                            <span class="text-warning"></span>
                                    </div>
                                    <div class="col">
                                        <br>
                                          <input type="submit" id="input-small" name="booking"  class="btn btn-primary" value="Simpan Alamat">
                                        </div>
                                  <div class="footer">                    
                                    <div class="container">
                                      <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col-6">
                                        </div>
                                        
                                      </div>
                                   
                                </div>
                            </form>
                    


                     </div> 
                </div>
            </div>
        </div>
  </div>
</section>

<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        
        // variabel dari nilai combo box unit
        var id_provinsi = $("#provinsi").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kabupaten.php",
            data: "provinsi_id="+id_provinsi,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kabupaten');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kabupaten").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>