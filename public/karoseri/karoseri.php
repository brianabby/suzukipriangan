<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h2>KAROSERI</h2>
            </div>
        </div>
    </div>
    <div class="row mb-50 mt-50">  
        <div class="row">
        <?php
            
            $sql = mysqli_query ($koneksi, "select jenis_karoseri, harga, id_karoseri from karoseri group by jenis_karoseri");
            while ($data = mysqli_fetch_array ($sql)){
        ?>
            <div class="col-md-3 d-flex align-items-stretch">
                <div class="card mb-4 shadow-sm">
                    <div class="gambar">
                        <img src="img/karoseri/<?php echo $data['img'];?>" class="img img-responsive" alt="<?php echo $data['img'];?>">
                        <div class="card-body">
                            <p class="card-text"><?php echo ucwords($data['jenis_karoseri']);?></p>
                            <p class="text-right">Mulai Dari <strong class="text-primary"><?php echo "Rp ".number_format($data['harga']);?></strong></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="#" class="btn btn-md btn-outline-secondary">
                                        <i class="fa fa-shopping-basket"></i> Shop
                                    </a>
                                </div>
                                <div class="btn-group">
                                    <a href="img/karoseri/<?php echo $data['spesifikasi']; ?>" class="btn btn-md btn-outline-secondary">
                                        <i class="fa fa-info"></i> Detail
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        <?php
        }
        ?>
        </div>
        <!-- album 1 -->
    </div>
</section>