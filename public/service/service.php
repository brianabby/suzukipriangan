<?php

    if (!defined ("INDEX")) {
        
        echo '<br><br><br><div class="row justify-content-md-center mt-5 wow fadeInUp" data-wow-delay="200ms">
        <div class="col-md-6">
            <div class="card mb-100">
                <div class="card-body">
                            <div class="alert alert-secondary" role="alert">
  <h4 class="alert-heading">Maaf :(</h4>
  <p>Untuk dapat mengakses halaman ini, Anda diharuskan melakukan login terlebih dahulu.</p>
  <hr>
  <p class="mb-0">Atau silahkan melakukan registrasi jika Anda belum mempunyai akun.</p>
</div>
                    </div>                
                </div>
            </div>
        </div><br><br><br>';
    }else{
    include ("lib/koneksi.php");

?> 
<section>
  <div class="container">
      <div class="service">
        <div class="row">
            <div class="col-12 mt-5">
                <!-- Section Heading -->
                <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                    <div class="line"></div>
                    <h2>Booking Service</h2>
                    <br>
                    <div class="line"></div>
                </div>
            </div>
        </div>
         <div class="row md-30 mt-50">
            <div class="col-md-8 offset-sm-2">
               <form action="?tampil=service_proses" method="POST">

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputNama4">Nama*</label>
                        <input type="text" class="form-control" id="inputNama4" placeholder="Nama" name="nama">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputPassword4">Email*</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email">
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputTelephon4">No. Hp</label>
                        <input type="text" class="form-control" id="inputTelephon4" placeholder="No. Handphone" name="hp">
                     </div>
                     <div class="form-group col-md-6">
                     <label for="inputTelephon4">Whatsapp</label>
                        <input type="text" class="form-control" id="inputTelephon4" placeholder="No. Whatsapp" name="wa">
                     </div>
                  </div>
                  
                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputKendaraan4">Pilih Kendaraan*</label>
                        <select id="merk_mobil" class="form-control" name="merk">
                            <option>Pilih kendaraan Anda</option>
                            <?php
                            $q=mysqli_query($koneksi, "SELECT distinct merk_mobil FROM mobil");
                            while($data_merk=mysqli_fetch_array($q)){
        
                            ?>
                            <option value="<?php echo $data_merk["merk_mobil"] ?>"><?php echo $data_merk["merk_mobil"] ?></option>
    
                            <?php
                            }
                            ?>
                        </select>
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputTipe4">Tipe Kendaraan*</label>
                        <select id="tipe" class="form-control" name="tipe">
                        </select>
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputVIN4">Nomor VIN*</label>
                        <input type="text" class="form-control" id="inputVIN4" placeholder="15 - 18 Digit nomor rangka kendaraan" name="no_vin">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputPolisi4">Nomor Polisi*</label>
                        <input type="text" class="form-control" id="inputPolisi4" placeholder="" name="no_pol">
                     </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputSTNK">Nama Pada STNK*</label>
                        <input type="text" class="form-control" id="inputSTNK" name="nama_stnk">
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputLayanan">Layanan*</label>
                        <select id="inputLayanan" class="form-control" name="layanan">
                           <?php
                           $layanan = array(
                              "Free Service 1 (1.000 KM)","Free Service 2 (5.000 KM)","Free Service 3 (10.000 KM)","Free Service 4 (20.000 KM)","Free Service 5 (30.000 KM)","Free Service 6 (40.000 KM)","Free Service 7 (50.000 KM)",
                              "Paket A (Kelipatan 5.000 KM)","Paket B (Kelipatan 10.000 KM)","Paket C (Kelipatan 20.000 KM)", "Paket D (Kelipatan 40.000 KM)", "Suzuki Product Quality Update", "Custom Service"
                           );
                           foreach ($layanan as $value) {
                              echo "<option value=".$value.">".$value."</option>";
                           }
                           ?>
                        </select>
                     </div>
                  </div>

                  <div class="form-row">
                      <div class="form-group col-md-6">
                          <label for="exampleFormControlSelect1">Kabupaten</label>
                          <select class="form-control" id="kabupaten" name="kabupaten">
                              <?php
                              $q=mysqli_query($koneksi, "SELECT distinct kabupaten FROM kabupaten order by kabupaten");
                              while($data_kab=mysqli_fetch_array($q)){

                              ?>
                              <option value="<?php echo $data_kab["kabupaten"] ?>"><?php echo $data_kab["kabupaten"] ?></option>
    
                              <?php
                              }
                              ?>
                          </select>
                      </div>
                      <div class="form-group col-md-6">
                          <label for="exampleFormControlSelect1">Kecamatan</label>
                          <select class="form-control" id="kecamatan" name="kecamatan">
                          </select>
                      </div>
                  </div>

                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="inputTanggal">Tanggal Service*</label>
                        <div class="">
                        <input type="date" class="form-control" id="inputTanggal" name="tgl">
                        </div>
                     </div>
                     <div class="form-group col-md-6">
                        <label for="inputJam">Jam Service*</label>
                        <select id="inputJam" class="form-control" name="jam_service">
                            <?php
                            $i = 8;
                            while($i <= 21){
                                $jam = $i.":00";
                                echo "<option value=".$jam.">".$jam."</option>";
                                $i++;
                            }
                            ?>
                        </select>
                     </div>
                  </div>
                      <br>

                  <button type="submit" class="btn btn-primary" name="booking_service">Daftar Service</button>

               </form>
                   <br>
            </div>
         </div>
      </div>
  </div>
</section>
<?php
    }
?>

<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        
        // variabel dari nilai combo box unit
        var id_provinsi = $("#provinsi").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kabupaten.php",
            data: "provinsi_id="+id_provinsi,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kabupaten');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kabupaten").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#merk_mobil").change(function(){
        
        // variabel dari nilai combo box unit
        var id_merk = $("#merk_mobil").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/tipe.php",
            data: "merk_id="+id_merk,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada tipe mobil');
                }
                
                // jika dapat mengambil data,, tampilkan di combo box
                else{
                    $("#tipe").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>