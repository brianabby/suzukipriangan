<!-- ##### Services Area Start ###### -->
    <section class="services-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
                        <h2>Mengapa Harus Kami?</h2>
                        <!-- <p>Our services</p> -->
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="200ms">
                        <div class="icon">
                            <i class="icon-money-1"></i>
                        </div>
                        <div class="text">
                            <h5>Jaminan Harga Terendah</h5>
                            <p>Temukan penawaran harga promo, DP serta cicilan terendah untuk mobil impian Anda</p>
                        </div>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="300ms">
                        <div class="icon">
                            <i class="icon-coin"></i>
                        </div>
                        <div class="text">
                            <h5>Mudah dan Nyaman</h5>
                            <p>Nikmati layanan otomotif dalam satu genggaman di gadget anda. Kami akan mendampingi mulai dari proses pembelian, perawatan, sampai pada saat Anda mengganti mobil lama Anda dengan produk terbaru</p>
                        </div>
                    </div>
                </div>

                <!-- Single Service Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-service-area d-flex mb-100 wow fadeInUp" data-wow-delay="400ms">
                        <div class="icon">
                            <i class="icon-smartphone-1"></i>
                        </div>
                        <div class="text">
                            <h5>Cepat dan Praktis</h5>
                            <p>Waktu Anda sangatlah berharga. Proses yang kami lakukan telah tersistem dan otomatis, sehingga Anda tidak perlu terganggu oleh telepon dari sales atau survei yang berulang-ulang. Mobil Anda akan dikirimkan paling lambat 3 hari setelah pengajuan disetujui</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Services Area End ###### -->