   <section class="team-area section-padding-100-0 miscellaneous-area bg-gray ">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="section-heading text-center mb-100">
                        <h2>Cara Mengajukan Kredit di Website Suzuki Priangan</h2>
                        <!-- <p>Our team</p> -->
                    </div>
                </div>
            </div>


            <div class="row">
                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-01.png" alt="" class="image-fluid mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Untuk mendapatkan pendampingan kredit, klik banner kredit lalu isi dan lengkapi kolom persyaratan yang telah disediakan</p>
                        </div>
                    </div>
                </div>

                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-02.png" alt="" class="mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Sales Kami akan menghubungi untuk mendampingi anda dengan memilih skema kredit sesuai dengan kebutuhan Anda dan mengajukan proses survei berdasarkan jadwal yang telah disepakati</p>
                        </div>
                    </div>
                </div>

                <!-- Single Team Member Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-team-member-area mb-100">
                        <div class="text-center">
                            <img src="https://suzukipriangan.com/img/core-img/icon-large-03.png" alt="" class="mx-auto d-block">
                            <!-- View More -->
                        </div>
                        <div class="team-info text-center mt-4">
                            <p>Setelah permintaan kredit disetujui, mobil akan dikirimkan ke alamat Anda dan plat mobil akan dikirimkan setelahnya</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- ##### Call To Action End ###### -->