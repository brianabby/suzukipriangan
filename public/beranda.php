  <!-- carousel start -->
  <div id="carouselExampleControls" class="carousel slide mt-5" data-ride="carousel">
    <div class="carousel-inner">
      <div class="container">
        <div class="carousel-item active">

          <div class="row pt-5 justify-content-center">
            <div class="col-9 col-sm-4 col-md-6 col-lg-5">
              <h1 class="mb-4 mt-4">Kredit Mobil Di Suzuki Priangan</h1>
              <p class="mb-4">Beli kendaraan di suzuki priangan</p>
              <a href="" class="btn btn-primary text-white">Kredit Mobil Baru</a>
            </div>
            <div class="col-3 col-sm-6 col-md-4 col-lg-4 d-none d-sm-block offset-1">
              <img src="img/slideshow/1.png" class="img-fluid">
            </div>
          </div>

        </div>
        <div class="carousel-item">
          <div class="row pt-5 justify-content-center ">
            <div class="col-9 col-sm-4 col-md-6 col-lg-5">
              <h1 class="mb-4 mt-4">Kredit Mobil Di Suzuki Priangan</h1>
              <p class="mb-4">JBeli kendaraan di suzuki priangan</p>
              <a href="" class="btn btn-primary text-white">Kredit Mobil Baru</a>
            </div>
            <div class="col-3 col-sm-6 col-md-4 col-lg-4 d-none d-sm-block offset-1">
              <img src="img/slideshow/1.png" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="row pt-5 justify-content-center">
            <div class="col-9 col-sm-4 col-md-6 col-lg-5">
              <h1 class="mb-4 mt-4">Kredit Mobil Di Suzuki Priangan</h1>
              <p class="mb-4">Beli kendaraan di suzuki priangan</p>
              <a href="" class="btn btn-primary text-white">Kredit Mobil Baru</a>
            </div>
            <div class="col-3 col-sm-6 col-md-4 col-lg-4 d-none d-sm-block offset-1">
              <img src="img/slideshow/2.png" class="img-fluid">
            </div>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!-- carousel end -->


  <!-- start -->
  <section class="features bg-light">


    <div class="container-fluid ">
      <div class="row text-center p-5">
        <div class="col">
          <a href="">
            <img src="img/features/features-web-shop.png" alt="" width="250" height="150">
          </a>
        </div>
        <div class="col">
          <a href="">
            <img src="img/features/features-web-service.png" alt="" width="250" height="150">
          </a>
        </div>
        <div class="col">
          <a href="">
            <img src="img/features/features-web-forum.png" alt="" width="250" height="150">
          </a>
        </div>
        <div class="col">
          <a href="">
            <img src="img/features/features-web-hotdeals.png" alt="" width="250" height="150">
          </a>
        </div>
      </div>
    </div>
  </section>
  <!-- end -->


  <!-- start -->
  <div class="container" style="margin-top:100px">
    <div class="row mb-3">
      <div class="col">
        <h3>CARA KREDIT & WHYUS</h3>
        <p>Spesial Khusus Untuk Anda</p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="card">
          <img src="img/brand/Group 192.png" alt="">
          <a href="#" class="btn btn-primary">Selengkapnya</a>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card">
          <img src="img/brand/Group 193.png" alt="">
          <a href="#" class="btn btn-primary">Selengkapnya</a>
        </div>
      </div>
    </div>
  </div>
  <!-- end -->


  <!-- start iklan -->
  <div class="container" style="margin-top:100px">

  <div class="col">
        <h3>IKLAN</h3>
        <p>Spesial Khusus Untuk Anda</p>
      </div>
  <div id="carouselExampleControls" class="carousel-iklan slide" data-ride="carousel" >
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/iklan/baleno 4.png" alt="First slide" height="195px" height="200px" >
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/iklan/baleno 3.png" alt="Second slide">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/iklan/baleno 2.png" alt="Third slide">
      </div>
    </div>
  </div>
  </div>
  <!-- end iklan -->

  <!-- start -->
  <section class="bg-light " style="margin-top:100px" >

    <div class="container mt-5 ">
      <div class="row">
        <div class="">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="mobil-tab" data-toggle="tab" href="#mobil" role="tab" aria-controls="mobil"
                aria-selected="true">MOBIL</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review"
                aria-selected="false">AKSESORIS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="sparepart-tab" data-toggle="tab" href="#sparepart" role="tab"
                aria-controls="sparepart" aria-selected="false">SPAREPART</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="karoseri-tab" data-toggle="tab" href="#karoseri" role="tab"
                aria-controls="karoseri" aria-selected="false">KAROSERI</a>
            </li>
          </ul>
          <!-- start mobil -->
          <div class="tab-content mt-4 " id="myTabContent">
            <div class="tab-pane fade show active product-review" id="mobil" role="tabpanel"
              aria-labelledby="mobil-tab">
              <div class="container">

                <!-- start mobil -->
                <div class="row">
                  <div class="col-sm-3">
                    <figure class="figure">
                      <a href="">
                        <img src="img/mobil/Baleno 3.png" class="figure-img img-fluid">
                      </a>
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 4.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>White Pure</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 5.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-sm-3">
                    <figure class="figure">
                      <img src="img/mobil/Baleno 6.png" class="figure-img img-fluid">
                      <figcaption class="figure-caption">
                        <div class="row">
                          <div class="col">
                            <h6>Hatty Bomb</h6>

                          </div>
                          <div class="col align-items-center d-flex justify-content-end">
                            <p style="font-size: 14px;">IDR. 209.000</p>
                          </div>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                </div>
                <!-- end mobil -->


              </div>
            </div>
            <!-- end -->

            <!-- aksesoris -->
            <div class="tab-pane fade product-review" id="review" role="tabpanel" aria-labelledby="review-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/VLEG.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/SPION.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/BUMPER.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/aksesoris/990J0M66R07-470.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->

            <!-- start sparepart  -->
            <div class="tab-pane fade product-sparepart" id="sparepart" role="tabpanel" aria-labelledby="sparepart-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart1.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart2.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart4.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->

            <!-- start karoseri -->
            <div class="tab-pane fade product-karoseri" id="karoseri" role="tabpanel" aria-labelledby="sparepart-tab">
              <div class="row">
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/karoseri/Carry 3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hty Bomb</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart2.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>White Pure</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart3.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>Hatty Bomb</h6>
                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>
                <div class="col-sm-3">
                  <figure class="figure">
                    <img src="img/sparepart/sparepart4.png" class="figure-img img-fluid">
                    <figcaption class="figure-caption">
                      <div class="row">
                        <div class="col">
                          <h6>kambing hitam</h6>

                        </div>
                        <div class="col align-items-center d-flex justify-content-end">
                          <p style="font-size: 14px;">IDR. 209.000</p>
                        </div>
                      </div>
                    </figcaption>
                  </figure>
                </div>

              </div>
            </div>
            <!-- end -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- end -->

  <!-- start artikel -->
  <div class="container" style="margin-top:100px">
    <div class="row">
      <div class="col artikel">
        <h3>Artikel</h3>
        <p>Spesial Khusus Untuk Anda</p>
      </div>
      <div class="card-deck">
        <div class="card">
          <img class="card-img-top" src="img/artikel/Baleno 5.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Judul Dari Artikel</h5>
            <small class="text-muted">This is a wider card with supporting text below as a natural lead-in to additional
              content. </small>
          </div>
          <div class="card-footer">
            <a href="">
              <small class="text-muted">More....</small>
            </a>
          </div>
        </div>
        <div class="card">
          <img class="card-img-top" src="img//artikel/Baleno 6.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Judul Dari Artikel</h5>
            <small class="text-muted">This is a wider card with supporting text below as a natural lead-in to additional
              content. </small>
          </div>
          <div class="card-footer">
            <a href="">
              <small class="text-muted">More....</small>
            </a>
          </div>
        </div>
        <div class="card">
          <img class="card-img-top" src="img/artikel/Baleno 4.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">Judul Dari Artikel</h5>
            <small class="text-muted">This is a wider card with supporting text below as a natural lead-in to additional
              content.</small>
          </div>
          <div class="card-footer">
            <a href="">
              <small class="text-muted">More....</small>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end artikel -->