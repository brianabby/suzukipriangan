<?php

    if (!defined ("INDEX")) {
        
        echo '<br><br><br><div class="row justify-content-md-center mt-5 wow fadeInUp" data-wow-delay="200ms">
        <div class="col-md-6">
            <div class="card mb-100">
                <div class="card-body">
                            <div class="alert alert-secondary" role="alert">
  <h4 class="alert-heading">Maaf :(</h4>
  <p>Untuk dapat mengakses halaman ini, Anda diharuskan melakukan login terlebih dahulu.</p>
  <hr>
  <p class="mb-0">Atau silahkan melakukan registrasi jika Anda belum mempunyai akun.</p>
</div>
                    </div>                
                </div>
            </div>
        </div><br><br><br>';
    }else{
    include ("lib/koneksi.php");

?>  
<div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h2>Pengajuan Test Drive</h2>
                <br>
                <div class="line"></div>
                <p class="text-center mb-3">Silahkan isi form di bawah ini untuk melakukan test drive atau konsultasi pembelian mobil Suzuki impian Anda. Mitra Suzuki akan menghubungi Anda untuk membantu Anda lebih lanjut.</p>
            </div>
        </div>
    </div>
<section class="mb-5">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col">
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-box">
                            <section class="content">
                                <form action="?tampil=test-drive_proses" method="POST">
                                <div class="row">
                                    <div class="col-sm">
                                                <div class="box box-info">
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                        <label>Nama</label>
                                                            <input id="nama" type="text" name="nama" class="form-control" placeholder="Nama" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label >Email</label>
                                                            <input id="email" type="email" name="email" class="form-control" id="inputEmail4MD" placeholder="Email" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                    
                                                        <div class="form-group">
                                                        <label>No Telepon / HP</label>
                                                            <input id="telepon" type="text" name="telepon" class="form-control" placeholder="No Telepon / HP" required textbox/>
                                                            <span class="form-control-feedback"></span>
                                                            <span class="text-warning"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="box box-info">
                                                    <div class="box-body">
                                                        <div class="form-group">
                                                            <label>Kabupaten</label>
                                                            <select class="form-control" name="kabupaten" id="kabupaten">
                                                            <?php
                                                            $q=mysqli_query($koneksi, "SELECT distinct kabupaten FROM kabupaten order by kabupaten");
                                                            while($data_kab=mysqli_fetch_array($q)){
   
                                                                ?>
                                                            <option value="<?php echo $data_kab["kabupaten"] ?>"><?php echo $data_kab["kabupaten"] ?></option>
    
                                                            <?php
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                                <label>Kecamatan</label>
                                                                <select class="form-control" name="kecamatan" id="kecamatan">
                                                                </select>
                                                        </div>
                                                                <div class="form-group">
                                                                <label>Alamat</label>
                                                                    <textarea rows="3" name="alamat" class="form-control" placeholder="Alamat"/></textarea>
                                                                </div>
                                                               
                                                    </div>
                                                </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    
                     </div> 
                     
                     <div class="col-md-6">
                        <div class="form-box" id="login-box">
                                <div class="box box-info">
                                                    <div class="form-head">
                                                    </div>
                                <div class="box-body">
                                        <div class="form-group">
                                            <label>Produk</label>
                                            <select class="form-control" name="produk">
                                                <?php
                                                $q=mysqli_query($koneksi, "SELECT distinct merk_mobil FROM mobil");
                                                while($data_prov=mysqli_fetch_array($q)){
    
                                                ?>
                                                <option value="<?php echo $data_prov["merk_mobil"] ?>"><?php echo $data_prov["merk_mobil"] ?></option>
    
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                          <label for="exampleFormControlTextarea1">Pertanyaan / Pesan</label>
                                          <textarea class="form-control rounded-0" rows="10" name="pertanyaan" required></textarea>
                                        </div>
                                   <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="setuju" id="inlineRadio1" value="setuju" required>
                                        <label class="form-check-label" for="inlineCheckbox1">Saya setuju dengan <a href="#">Syarat & Ketentuan</a> yang berlaku</label><br>
                                </div>
                                    <div class="col">
                                        <br>
                                          <input type="submit" id="input-small" name="booking"  class="btn btn-primary" value="Booking Test Drive">
                                        </div>
                                  <div class="footer">                    
                                    <div class="container">
                                      <div class="row">
                                        <div class="col">
                                        </div>
                                        <div class="col-6">
                                        </div>
                                        
                                      </div>
                                   
                                </div>
                            </form>
                    


                     </div> 
                </div>
            </div>
        </div>
  </div>
</section>
            <?php
    }
?>

<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
    $("#provinsi").change(function(){
        
        // variabel dari nilai combo box unit
        var id_provinsi = $("#provinsi").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kabupaten.php",
            data: "provinsi_id="+id_provinsi,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kabupaten');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kabupaten").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>
<script type="text/javascript">
    $("#kabupaten").change(function(){
        
        // variabel dari nilai combo box unit
        var id_kabupaten = $("#kabupaten").val();
        
        // tampilkan image load
        $("#imgLoad").show("");
              
        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/kecamatan.php",
            data: "kabupaten_id="+id_kabupaten,
            success: function(msg){
                   
                // jika tidak ada data
                if(msg == ''){
                    alert('Tidak ada Kecamatan');
                }
                
                // jika dapat mengambil data, tampilkan di combo box
                else{
                    $("#kecamatan").html(msg);                                                     
                }
                
                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });    
    });
</script>