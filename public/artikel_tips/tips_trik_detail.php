<?php
    include ("lib/koneksi.php");
    $sql = mysqli_query ($koneksi, "SELECT * FROM tips_trik WHERE id_tips_trik='$_GET[id]'");
    $tips_trik = mysqli_fetch_array ($sql);
?>
<section class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(../img/bg-img/21.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Detail Tips & Trik</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i>Home</i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><i>Detail Tips & Trik</i></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-news-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="post-details-content mb-100">
                        <h4><?php echo $tips_trik['judul'];?></h4>
                        <i><p>Dipost: <?php echo $tips_trik['penulis']; ?> | Tanggal : <?php echo $tips_trik['tgl_tulis']; ?></p></i>
                        <img src="../img/tips_trik/<?php echo $tips_trik['img_tips_trik']; ?>" alt=""><br>

                        <p align="justify"><?php echo $tips_trik['isi']; ?></p>
                    </div>
                </div>

                <!-- Sidebar Widget -->
                <div class="col-12 col-sm-9 col-md-6 col-lg-4">
                    <div class="sidebar-area mb-100">

                        <!-- Single Sidebar Widget -->
                        <div class="single-widget-area cata-widget">
                            <div class="widget-heading">
                                <div class="line"></div>
                                <h4>Press Release</h4>
                            </div>

                            <?php
                                $query = "SELECT * FROM tips_trik ORDER BY id_tips_trik desc limit 10";
                                $hasil = mysqli_query($koneksi, $query);
                                while ($tips_trikz = mysqli_fetch_assoc($hasil)) { ?>

                            <div class="single-news-area d-flex align-items-center">
                                <div class="news-thumbnail">
                                    <img src="../img/tips_trik/<?php echo $tips_trikz['img_tips_trik']; ?>" alt="">
                                </div>
                                <div class="news-content">
                                    <span><?php echo $tips_trikz['tgl_tulis']; ?></span>
                                    <a href="?tampil=tips_trik_detail&id=<?php echo $tips_trikz['id_tips_trik']; ?>"><?php echo $tips_trikz['judul']; ?></a>
                                    <div class="news-meta">
                                        <a href="#" class="post-author"> Penulis</a>
                                        <a href="#" class="post-date"> <?php echo $tips_trikz['penulis']; ?></a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>