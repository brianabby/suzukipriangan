    <?php
        include ("lib/koneksi.php");
        $sql = mysqli_query ($koneksi, "SELECT * FROM artikel WHERE id_artikel='$_GET[id]'");
        $artikel = mysqli_fetch_array ($sql);
    ?>
    
    <section class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/13.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Artikel</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i>Home</i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><i>Artikel</i></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### News Area Start ##### -->
    <section class="post-news-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <?php 
                       include "data_artikel1.php"; 
                       foreach ($data_artikel as $artikel):
                    ?>
                    <div class="single-blog-area mb-70">
                        <div class="blog-thumbnail">
                            <a href="?tampil=artikel_detail&id=<?php echo $artikel['id_artikel']; ?>">
                                <img src="img/artikel/<?php echo $artikel['img_artikel']; ?>" alt="">
                            </a>
                        </div>
                        <div class="blog-content">
                            <span><?php echo $artikel['tgl_tulis']; ?></span>
                            <a href="?tampil=artikel_detail&id=<?php echo $artikel['id_artikel']; ?>" class="post-title"><?php echo $artikel['judul']; ?></a>
                            <div class="blog-meta">
                                <a href="#" class="post-author"> <?php echo $artikel['penulis']; ?></a>
                                <a href="#" class="post-date"> <?php echo $artikel['tgl_tulis']; ?></a>
                            </div>
                            <p><?php echo substr($artikel['isi'],0,200); ?>..</p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### News Area End ##### -->