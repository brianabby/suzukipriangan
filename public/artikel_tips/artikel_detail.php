<?php
    include ("lib/koneksi.php");
    $id = mysqli_real_escape_string($koneksi, $_GET[id]);
    $sql = mysqli_query ($koneksi, "SELECT * FROM artikel WHERE id_artikel='$id'");
    $artikel = mysqli_fetch_array ($sql);
?>
    <section class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(../img/bg-img/21.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Detail Artikel</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i>Home</i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><i>Detail Artikel</i></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### Post Details Area Start ##### -->
    <section class="post-news-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="post-details-content mb-100">
                        <h4><?php echo $artikel['judul'];?></h4>
                        <i><p>Dipost: <?php echo $artikel['penulis']; ?> | Tanggal : <?php echo $artikel['tgl_tulis']; ?></p></i>
                        <img src="../img/artikel/<?php echo $artikel['img_artikel']; ?>" alt=""><br>

                        <p align="justify"><?php echo $artikel['isi']; ?></p>
                    </div>
                </div>

                <!-- Sidebar Widget -->
                <div class="col-12 col-sm-9 col-md-6 col-lg-4">
                    <div class="sidebar-area mb-100">

                        <!-- Single Sidebar Widget -->
                        <div class="single-widget-area cata-widget">
                            <div class="widget-heading">
                                <div class="line"></div>
                                <h4>Press Release</h4>
                            </div>

                            <?php
                                $query = "SELECT * FROM artikel ORDER BY id_artikel desc limit 10";
                                $hasil = mysqli_query($koneksi, $query);
                                while ($artikelz = mysqli_fetch_assoc($hasil)) { ?>

                            <div class="single-news-area d-flex align-items-center">
                                <div class="news-thumbnail">
                                    <img src="../img/artikel/<?php echo $artikelz['img_artikel']; ?>" alt="">
                                </div>
                                <div class="news-content">
                                    <span><?php echo $artikelz['tgl_tulis']; ?></span>
                                    <a href="?tampil=artikel_detail&id=<?php echo $artikelz['id_artikel']; ?>"><?php echo $artikelz['judul']; ?></a>
                                    <div class="news-meta">
                                        <a href="#" class="post-author"> Penulis</a>
                                        <a href="#" class="post-date"> <?php echo $artikelz['penulis']; ?></a>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### News Area End ##### -->