    <section class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url(img/bg-img/13.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2>Tips & Trik</h2>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><i>Home</i></a></li>
                                <li class="breadcrumb-item active" aria-current="page"><i>Tips & Trik</i></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Breadcrumb Area End ##### -->

    <!-- ##### News Area Start ##### -->
    <section class="news-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-7">
                    <?php 
                       include "data_tips_trik.php"; 
                       foreach ($data_tips_trik as $tips_trik):
                    ?>
                    <div class="single-blog-area mb-70">

                        <div class="blog-thumbnail">
                            <a href="?tampil=tips_trik_detail&id=<?php echo $tips_trik['id_tips_trik']; ?>">
                                <img src="img/tips_trik/<?php echo $tips_trik['img_tips_trik']; ?>" alt="">
                            </a>
                        </div>
                        <div class="blog-content">
                            <span><?php echo $tips_trik['tgl_tulis']; ?></span>
                            <a href="?tampil=tips_trik_detail&id=<?php echo $tips_trik['id_tips_trik']; ?>" class="post-title"><?php echo $tips_trik['judul']; ?></a>
                            <div class="blog-meta">
                                <a href="#" class="post-author"> <?php echo $tips_trik['penulis']; ?></a>
                                <a href="#" class="post-date"> <?php echo $tips_trik['tgl_tulis']; ?></a>
                            </div>
                            <p><?php echo substr($tips_trik['isi'],0,200); ?>..</p>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
                

                <!-- Sidebar Area -->
                <div class="col-12 col-lg-4">
                    
                </div>

            </div>
        </div>
    </section>
    <!-- ##### News Area End ##### -->