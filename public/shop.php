
<section class="container">
    <div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h2>SHOP</h2>
            </div>
        </div>
    </div>
    <div class="row my-5">
        <div class="col-12 col-sm-6 col-lg-4">
            <a href="https://www.suzukipriangan.com/?tampil=mobil" class="btn btn-block btn-primary" style="background-image: url('../img/core-img/bg-shop-mobil.png'); 
            background-repeat:no-repeat;
            height:100%;
            background-position:center;
            background-size:cover;
            border:none;
            ">
                <h3 class="h3 text-left py-5">Mobil</h3>
            </a>
        </div>

        <div class="col-12 col-sm-6 col-lg-4">
           <a href="https://www.suzukipriangan.com/?tampil=aksesoris_kategori" class="btn btn-block btn-primary" style="background-image: url('../img/core-img/bg-shop-aksesoris.png'); 
            background-repeat:no-repeat;
            height:100%;
            background-position:center;
            background-size:cover;
            border:none;
            ">
                <h3 class="h3 text-left py-5">Aksesoris</h3>
            </a>
        </div>

        <div class="col-12 col-sm-12 col-lg-4">
            <a href="https://www.suzukipriangan.com/?tampil=sparepart" class="btn btn-block btn-primary" style="background-image: url('../img/core-img/bg-shop-spareparts.png'); 
            background-repeat:no-repeat;
            height:100%;
            background-position:center;
            background-size:cover;
            border:none;
            ">
                <h3 class="h3 text-left py-5">Sparepart</h3>
            </a>
        </div>
    </div>

</section>





