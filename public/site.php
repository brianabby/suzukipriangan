<!-- ##### Features Area End ###### -->
    <section class="about-area section-padding-100-0">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="about-thumbnail mb-100">
                        <a href="https://suzukipriangan.com/?tampil=carakredit">
                            <img src="img/why/kreditmobil.jpg" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="about-thumbnail mb-100">
                        <a href="https://suzukipriangan.com/?tampil=whyus">
                            <img src="img/why/mengapaya.jpg" alt="">    
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>