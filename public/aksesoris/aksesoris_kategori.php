<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h3>AKSESORIS | KATEGORI</h3>
            </div>
        </div>
    </div>
    <div class="row mb-50 mt-50">  
        <div class="row">
        <?php
            
            $sql = mysqli_query ($koneksi, "SELECT kategori, gambar_acc FROM aksesoris group by kategori");
            while ($data = mysqli_fetch_array ($sql)){
        ?>
            <div class="col-md-3 col-xs-6 col-sm-6 d-flex align-items-stretch">
                <div class="card mb-3 shadow-sm">
                    <div class="gambar">
                        <a href="?tampil=aksesoris&id=<?php echo $data['kategori']; ?>"><img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" class="img img-responsive" style="height:200px;" alt="<?php echo $data['gambar_acc'];?>">
                            <br><br>
                        <p class="text-center"><small><?php echo $data['kategori'];?></small></p>
                        </a>
                    </div>
                </div>
            
            </div>
        <?php
        }
        ?>
        </div>
        <!-- album 1 -->
    </div>
</section>