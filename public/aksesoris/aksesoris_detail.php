<?php

    include ("lib/koneksi.php");
    $id = mysqli_real_escape_string($koneksi, $_GET[id]);
    $tes = mysqli_query ($koneksi, "SELECT * FROM aksesoris where id_aksesoris='$id'");
    $aksesoris = mysqli_fetch_array ($tes);

?>
<section class="container">
    <div class="container">
        <div class="row mb-5 mt-5">
            <div class="col-md-12">
                <div id="sukses">
                    
                </div>
                <p><a href="?tampil=beranda">Beranda</a> > <a href="?tampil=aksesoris_kategori">Aksesoris</a> > <a href="?tampil=aksesoris&id=<?php echo $aksesoris['kategori'];?>">
                    <?php echo $aksesoris['kategori']; ?></a> > <strong><?php echo $aksesoris['nama_acc']; ?></strong></p>
                <div class="row">
                    <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="item">
                                        <div class="preview">
                                            <img class="img-responsive" 
                                            src="img/aksesoris/<?php echo $aksesoris['gambar_acc'];?>" 
                                            alt="Card image"
                                            style="width:100%; height:auto;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5><?php echo $aksesoris['nama_acc']; ?></h5>
                                <p><?php echo $aksesoris['merk_acc']; ?></p>
                                <hr>
                                <h5><strong class="text-primary"><?php echo "Rp ".number_format($aksesoris['harga_acc'])?></strong></h5>
                                <br>
                                <form method="post" class="form-keranjang" action="?tampil=keranjang_proses">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <input type="text" class="form-control" id="id_aksesoris" name="id_aksesoris" value="<?php echo $aksesoris['id_aksesoris']; ?>" hidden>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" class="form-control" id="id_konsumen" name="id_konsumen" value="<?php echo $konsumen['id_konsumen']; ?>" hidden>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $aksesoris['nama_acc']; ?>" hidden>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="text" class="form-control" id="harga" name="harga" value="<?php echo $aksesoris['harga_acc']; ?>" hidden>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <p><strong>Banyak</strong></p>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="number" class="form-control" id="banyak" name="banyak" value="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                            if (!defined ("INDEX")){
                                                ?>
                                        <button type="button" class="btn btn-outline-info btn-block" data-toggle="modal" data-target="#login" id="submitBtn"><i class="fa fa-cart-plus"></i><span> Masukkan Keranjang</span></button>
                                        <?php
                                            }else{
                                                ?>
                                        <button type="submit" class="btn btn-outline-info btn-block" id="masukkan-keranjang"><i class="fa fa-cart-plus"></i><span> Masukkan Keranjang</span></button>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </form>
                            </div>
                            </div>
    
                        </div>
                        </div>
                        <br>
                        <div class="card">
                            <div class="card-header">
                                <h6>Lain -lain</h6>
                            </div>

                            <div class="card-body">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#home">
                                            Spesifikasi
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu1">
                                            Deskripsi
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu2">
                                            Ulasan
                                        </a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->

                                <div class="tab-content">
                                    <div class="tab-pane container active" id="home">
                                    </div>

                                    <div class="tab-pane container fade" id="menu1">
                                    </div>

                                    <div class="tab-pane container fade" id="menu2">
                                        <br><br>
                                        <h3 class="text-center">Belum ada ulasan</h3>
                                        <br>
                                        <p class="text-center">
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-4">
                        <h6><i>Produk Serupa</i></h6>
                        <hr>
                        <div class="row">
                            <?php
            
                            $sql = mysqli_query ($koneksi, "SELECT nama_acc, gambar_acc, id_aksesoris FROM aksesoris where kategori='$aksesoris[kategori]' and  id_aksesoris not in ('$aksesoris[id_aksesoris]') order by rand() limit 4");
                                    while ($data = mysqli_fetch_array ($sql)){
                            ?>
                            <div class="col-md-6 d-flex align-items-stretch">
                                <div class="card mb-4 shadow-sm">
                                    <div class="gambar">
                                        <a href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>">
                                        <img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" class="img img-responsive" style="height:130px;" alt="<?php echo $data['gambar_acc'];?>">
                                            <br>
                                        <p class="text-center align-middle"><small><?php echo $data['nama_acc'];?></small></p>
                                        </a>
                                    </div>
                                </div>
            
                            </div>
                            <?php
                                    }
                            ?>
                        </div>
                    </div>
                </div>
                <!-- Komentar FB -->
                <br><br>
                <div class="fb-comments" data-href="https://suzukipriangan.com/docs/plugins/comments#configurator" data-width="700" data-numposts="5"></div>
                <!-- Komentar FB End -->
            </div>

        </div>

    </div>
</section>
<!--
<script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    $("#masukkan-keranjang").submit(function () {

        // variabel dari nilai combo box unit
        var id_aksesoris = $("#id_aksesoris").val();
        var id_tipe = $("#tipe").val();
        var id_tipe = $("#tipe").val();
        var id_tipe = $("#tipe").val();
        var id_tipe = $("#tipe").val();

        // tampilkan image load
        $("#imgLoad").show("");

        // mengirim dan mengambil data
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "public/harga.php",
            data: "id_aksesoris="+id_aksesoris,
            success: function (msg) {

                // jika tidak ada data
                if (msg == '') {
                    alert('Tidak ada harga');
                }

                // jika dapat mengambil data,, tampilkan di combo box tgl
                else {
                    $("#sukses").html(msg);
                }

                // hilangkan image load
                $("#imgLoadd").hide();
            }
        });
    });
</script>
<!--
<script type="text/javascript">
	$(document).ready(function(){
		$("#masukkan-keranjang").click(function(){
			var data = $('.form-keranjang').serialize();
            e.preventDefault();
			$.ajax({
				type: 'POST',
				url: 'public/keranjang_proses.php',
                data: data,
				success: function() {
					$('#sukses').load("public/keranjang_proses.php");
				}
			});
		});
	});
</script>
-->
<!--
<script type="text/javascript">
    $(document).ready(function){
        $('form').on('submit',function(e){
            e.preventDefault();
            $.ajax({
                type: $(this).Attr('method'),
                URL: $(this).Attr('action'),
                data: $(this).serialize(),
                success:function(msg) {

                // jika tidak ada data
                if (msg == '') {
                    alert('Tidak ada harga');
                }

                // jika dapat mengambil data,, tampilkan di combo box tgl
                else {
                    $("#2_harga").html(msg);
                }

                // hilangkan image load
                $("#imgLoadd").hide();
                }
            })
        })
    }
</script>
-->