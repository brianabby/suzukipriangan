<?php

    include ("lib/koneksi.php");

?>
<section class="container">
    <div class="row">
        <div class="col-12 mt-50">
            <!-- Section Heading -->
            <div class="section-heading text-center mb-3 wow fadeInUp" data-wow-delay="100ms">
                <div class="line"></div>
                <h4>Aksesoris | <?php echo $_GET[id]; ?></h4>
            </div>
        </div>
    </div>
    <div class="row mb-50 mt-50"> 
        <div class="row">
        <?php
            
            $sql = mysqli_query ($koneksi, "SELECT harga_acc, merk_acc, nama_acc, gambar_acc, id_aksesoris FROM aksesoris where kategori='$_GET[id]'");
            while ($data = mysqli_fetch_array ($sql)){
        ?>
            <div class="col-md-3 d-flex align-items-stretch">
                <div class="card mb-3 shadow-sm">
                    <div class="gambar">
                        <img src="img/aksesoris/<?php echo $data['gambar_acc'];?>" class="img img-responsive" style="height:200px;" alt="<?php echo $data['gambar_acc'];?>">
                        <div class="card-body">
                            <p class="text-left"><strong><?php echo $data['nama_acc'];?></strong><br>
                                <?php echo ucwords($data['merk_acc']);?></p>
                            <p class="text-right"><strong class="text-primary"><?php echo "Rp ".number_format($data['harga_acc']);?></strong></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="?tampil=aksesoris_detail&id=<?php echo $data['id_aksesoris'];?>" class="btn btn-outline-secondary btn-block">
                                        <i class="fa fa-shopping-basket"></i> Shop
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>
        <?php
        }
        ?>
        </div>
        <!-- album 1 -->
    </div>
</section>