<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/fontawesome.css">
  <link rel="stylesheet" href="css/solid.css">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:200,400,600&display=swap"
    rel="stylesheet">

  <!-- My CSS -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/logo.png">
  <title>SUZUKI PRIANGAN</title>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <?php include ("menu.php"); ?>
  </nav>
  <!-- Akhir Navbar -->
  <div class="isi">
      <?php include ("konten.php"); ?>
  </div>
  <!-- Footer start -->
  <hr style="margin-top: 200px;">
  <footer class="footer-area section-padding-100-0">
    <div class="container mt-5">
      <div class="row">

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">INFORMASI</h5>
            <nav>
              <ul>
                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=8">Tentang Kami</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=4">Cara Kerja</a></li>
                <li><a href="#">Kebijakan Privasi</a></li>
                <li><a href="#">Hubungin Kami</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">LAYANAN</h5>
            <nav>
              <ul>
                <li><a href="#">Pembelian</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=6">Perawatan</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=5">Pendampingan Kredit</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">KONTAK KAMI</h5>
            <!-- Single News Area -->
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-content">
                <a href="#">Customer Service</a>
                <div class="news-meta">
                  <a href="#" class="post-author"> Indah</a>
                  <a href="#" class="post-author"> +62 818-430-490</a>
                </div>
              </div>
            </div>

            <!-- Single News Area -->
          </div>
        </div>

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">SISTEM PEMBAYARAN</h5>
            <!-- Single News Area -->
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-visa.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-mastercard.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-jcb.png" alt="">
              </div>
            </div>
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-amex.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-alfa.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-info.png" alt="">
              </div>
            </div>
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-bank.png" alt="">
              </div>
            </div>
            <!-- Single News Area -->
          </div>
        </div>
      </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="copywrite-content d-flex flex-wrap justify-content-between align-items-center">
              <!-- Footer Logo -->
              <a href="index.php" class="footer-logo"></a>

              <!-- Copywrite Text -->
              <p class="copywrite-text"><a href="#">
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;
                  <script>document.write(new Date().getFullYear());</script> All rights reserved By <a
                    href="https://www.suzukipriangan.com" target="_blank">Suzuki Priangan</a>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <br><br>
  <!-- end footer -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/all.js"></script>
    <!-- Slick js -->
    <script src="js/slick/slick.min.js"></script>
    <script src="js/slick/slickSlider.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    </body>
</html>
<!-- Daftar start -->
  <div class="modal fade" id="daftar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">REGISTRASI</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form class="form-signin" role="form" method="POST" action="?tampil=registrasi_proses">
            <div class="text-center mb-4">
              <p class="mb-3 font-weight-normal">Daftar Anggota Baru</p>
            </div>

            <div class="form-group has-feedback">
              <input id="nama" type="text" class="form-control" placeholder="Nama Lengkap" name="nama_konsumen" required
                textbox>
              <span class="form-control-feedback"></span>
              <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
              <input id="nohp" type="number" class="form-control" placeholder="Nomor HP" name="phone_number"
                minlength="10" maxlength="12" required textbox>
              <span class="form-control-feedback"></span>
              <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
              <input id="email" type="email" name="email" class="form-control" placeholder="Email" required textbox>
              <span class="form-control-feedback"></span>
              <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
              <input class="form-control" id="password" name="password" type="password" pattern="^\S{6,}$"
                onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Minimal 6 Karakter' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;"
                placeholder="Password" required>
              <span class="form-control-feedback"></span>
              <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
              <input class="form-control" id="password_two" name="password_two" type="password" pattern="^\S{6,}$"
                onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Masukkan Password Yang Sama' : '');"
                placeholder="Verify Password" required>
              <span class="form-control-feedback"></span>
              <span class="text-warning"></span>
            </div>
            <button type="submit" name="submit" class="btn btn-md btn-primary btn-block mt-3"><i class="fa fa-send"></i>
              DAFTAR
            </button>
            <p class="mt-5 mb-3 text-muted text-center">&copy;
              <script>document.write(new Date().getFullYear());</script>
            </p>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- Daftar end -->

  <!-- login start -->
  <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">LOGIN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-signin" method="POST" action="?tampil=cek_login">
                        <div class="text-center mb-4">Silahkan Daftar jika belum punya akun</div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control" placeholder="Email" name="email" id="usernameLOG" textbox>
                            <span class="form-control-feedback"></span>
                            <span class="text-warning"></span>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="passwordLOG" textbox>
                            <span class="form-control-feedback"></span>
                            <span class="text-warning"></span>
                        </div>
                        <button type="submit" class="mb-3 btn btn-lg btn-primary btn-block" name="submit"><i class="fa fa-unlock"></i> Masuk</button>
                        <p class="mt-5 mb-3 text-muted text-center">&copy; <script>document.write(new Date().getFullYear());</script></p>
                    </form>
      </div>
    </div>
  </div>
</div>
    <!-- login end -->