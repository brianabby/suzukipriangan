<div class="container">
      <a class="navbar-brand" href="#">
        <img src="img/logo.png" alt="suzuki" width="60" height="40">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav text-uppercase mx-auto">
          <li class="nav-item">
            <a class="nav-link" href="?tampil=beranda">BERANDA</a>
          </li>
          <li class="nav-item  dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">Garasi</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="?tampil=mobil">MOBIL</a>
              <a class="dropdown-item" href="?tampil=aksesoris_kategori">AKSESORIS </a>
            </div>
          </li>
          <li class="nav-item  dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">Bengkel</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="?tampil=service">BOOKING SERVICE</a>
            </div>
          </li>

          <li class="nav-item  dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">CORNER</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="?tampil=artikel">ARTIKEL</a>
              <a class="dropdown-item" href="?tampil=tips_trik">TIPS AND TRIK</a>

            </div>
          </li>
          <li class="nav-item  dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">BAZAR</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="?tampil=promo">PROMO</a>
              <a class="dropdown-item" href="?tampil=event">EVENT</a>

            </div>
          </li>
          <?php
                if (!defined ("INDEX")) {
          ?>
          <li class="nav-item">
            <a href="" class="btn btn-outline-primary text-white nav-link " data-toggle="modal"
              data-target="#login">MASUK</a>
          </li>
          <li class="nav-item">
            <a href="" class="btn btn-outline-primary text-white nav-link " data-toggle="modal"
              data-target="#daftar">DAFTAR</a>
          </li>
          <?php
                } else {
          ?>
          <li class="nav-item  dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">Saya</a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="?tampil=profil">Profil</a>
              <a class="dropdown-item" href="?tampil=logout">Logout</a>

            </div>
          </li>
          <li><a href="?tampil=keranjang" class="mr-2" title="Keranjang Saya"><img src="http://suzukipriangan.com/img/core-img/shopping-cart.png" alt="" class="col-md-8 col-xs-8"></a></li>
          <?php
          }
          ?>
        </ul>
        <form class="form-inline">
          <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        </form>
      </div>
    </div>