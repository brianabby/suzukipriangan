<?php

    session_start();

    if (!empty ($_SESSION['email']) and ($_SESSION['password'])) {

    include ("lib/koneksi.php");
    define("INDEX", true);
        
    $sql = mysqli_query ($koneksi, "SELECT * FROM konsumen WHERE email='$_SESSION[email]'");
    $konsumen = mysqli_fetch_array ($sql);

?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/all.css">
  <link rel="stylesheet" href="css/fontawesome.css">
  <link rel="stylesheet" href="css/solid.css">

  <!-- Google Font -->
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:200,400,600&display=swap"
    rel="stylesheet">

  <!-- My CSS -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" href="img/logo.png">
  <title>SUZUKI PRIANGAN</title>
</head>

<body>

  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <?php include ("menu.php"); ?>
  </nav>
  <!-- Akhir Navbar -->
  <div class="isi">
      <?php include ("konten.php"); ?>
  </div>
  <!-- Footer start -->
  <hr style="margin-top: 200px;">
  <footer class="footer-area section-padding-100-0">
    <div class="container mt-5">
      <div class="row">

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">INFORMASI</h5>
            <nav>
              <ul>
                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=8">Tentang Kami</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=4">Cara Kerja</a></li>
                <li><a href="#">Kebijakan Privasi</a></li>
                <li><a href="#">Hubungin Kami</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">LAYANAN</h5>
            <nav>
              <ul>
                <li><a href="#">Pembelian</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=6">Perawatan</a></li>
                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=5">Pendampingan Kredit</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">KONTAK KAMI</h5>
            <!-- Single News Area -->
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-content">
                <a href="#">Customer Service</a>
                <div class="news-meta">
                  <a href="#" class="post-author"> Indah</a>
                  <a href="#" class="post-author"> +62 818-430-490</a>
                </div>
              </div>
            </div>

            <!-- Single News Area -->
          </div>
        </div>

        <!-- Single Footer Widget -->
        <div class="col-12 col-sm-6 col-lg-3">
          <div class="single-footer-widget mb-100">
            <h5 class="widget-title">SISTEM PEMBAYARAN</h5>
            <!-- Single News Area -->
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-visa.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-mastercard.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-jcb.png" alt="">
              </div>
            </div>
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-amex.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-alfa.png" alt="">
              </div> &nbsp; &nbsp;
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-info.png" alt="">
              </div>
            </div>
            <div class="single-latest-news-area d-flex align-items-center">
              <div class="news-thumbnail">
                <img src="https://moladin.com/images/footer-bank.png" alt="">
              </div>
            </div>
            <!-- Single News Area -->
          </div>
        </div>
      </div>
    </div>

    <!-- Copywrite Area -->
    <div class="copywrite-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="copywrite-content d-flex flex-wrap justify-content-between align-items-center">
              <!-- Footer Logo -->
              <a href="index.php" class="footer-logo"></a>

              <!-- Copywrite Text -->
              <p class="copywrite-text"><a href="#">
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;
                  <script>document.write(new Date().getFullYear());</script> All rights reserved By <a
                    href="https://www.suzukipriangan.com" target="_blank">Suzuki Priangan</a>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <br><br>
  <!-- end footer -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/all.js"></script>
    <!-- Slick js -->
    <script src="js/slick/slick.min.js"></script>
    <script src="js/slick/slickSlider.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    </body>
</html>
<?php
        
    }else{
         header ("location: index.php");
    }

?>