<!DOCTYPE html>
<html lang="en">

<head>
   

    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>
    <script src="https://www.google.com/recaptcha/api.js?render=reCAPTCHA_site_key"></script>
    
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>SUZUKI Priangan | PT Suzuki Indomobil</title>

    <!-- Favicon -->
    <link rel="icon" href="img/logo2.png">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">
    
    <!-- fa icon -->
    <link rel="stylesheet" href="css/fa/all.min.css">
     <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-145750661-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-145750661-1');
    </script>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <div class="top-header-area" style="height:50px; background:#003679;">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12 d-flex justify-content-between">

                    <!-- Logo Area -->
                    <div class="logo">

                    </div>
                    
                        <!-- Top Contact Info -->
                        <div class="top-contact-info d-flex align-items-center">
                    
                        <a class="mr-2" style="color:#ffc107; font-size:.875rem;" href="#" data-toggle="tooltip" data-placement="bottom" title="Jl Dr.Setiabudhi No.78 Pasteur"><img src="img/core-img/placeholder.png" alt=""></a>
                        <a class="mr-2" style="color:#ffc107; font-size:.875rem;" href="#" data-toggle="tooltip" data-placement="bottom" title="0818430490"><img src="img/core-img/message.png" alt=""></a>
                        
                        <?php
                            if (!defined ("INDEX")) {
                                echo '
                                    <button type="button" name="btn" class="btn btn-outline-warning btn-sm" data-placement="bottom" data-toggle="modal" data-target="#login" id="submitBtn">MASUK</button>&nbsp
                                    <button type="button" name="btn" class="btn btn-outline-warning btn-sm" data-placement="bottom" data-toggle="modal" data-target="#daftar" id="submitBtn">DAFTAR</button>';
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include ("menu.php"); ?>
    </header>
<?php include ("konten.php"); ?>
    <footer class="footer-area section-padding-100-0">
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">INFORMASI</h5>
                        <nav>
                            <ul>
                                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=8">Tentang Kami</a></li>
                                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=4">Cara Kerja</a></li>
                                <li><a href="#">Kebijakan Privasi</a></li>
                                <li><a href="#">Hubungin Kami</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">LAYANAN</h5>
                        <nav>
                            <ul>
                                <li><a href="#">Pembelian</a></li>
                                <li><a href="https://suzukipriangan.com/?tampil=artikel_detail&id=6">Perawatan</a></li>
                                <li><a href="https://suzukipriangan.com/?tampil=tips_trik_detail&id=5">Pendampingan Kredit</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">KONTAK KAMI</h5>
                        <!-- Single News Area -->
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="img/core-img/icon-large-02.png" alt="">
                            </div>
                            <div class="news-content">
                                <a href="#">Customer Service</a>
                                <div class="news-meta">
                                    <a href="#" class="post-author"> Indah</a>
                                    <a href="#" class="post-author"> +62 818-430-490</a>
                                </div>
                            </div>
                        </div>
                     
                        <!-- Single News Area -->
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">SISTEM PEMBAYARAN</h5>
                        <!-- Single News Area -->
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-visa.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-mastercard.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-jcb.png" alt="">
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-amex.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-alfa.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-info.png" alt="">
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-bank.png" alt="">
                            </div>
                        </div>
                        <!-- Single News Area -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Copywrite Area -->
        <div class="copywrite-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copywrite-content d-flex flex-wrap justify-content-between align-items-center">
                            <!-- Footer Logo -->
                            <a href="index.php" class="footer-logo"></a>

                            <!-- Copywrite Text -->
                            <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved By <a href="https://www.suzukipriangan.com" target="_blank">Suzuki Priangan</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->

    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    <!-- Slick js -->
    <script src="js/slick/slick.min.js"></script>
    <script src="js/slick/slickSlider.js"></script>
    <!-- Font-Awesome -->
    <script src="js/fa/all.min.js"></script>
    <!-- FB JS -->
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>

    <!-- JS for item -->


</body>

</html>
<!-- Login Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">LOGIN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-signin" method="POST" action="?tampil=cek_login.php">
                        <div class="text-center mb-4">Silahkan Daftar jika belum punya akun</div>
                        <div class="form-group mb-4">
                            <input type="text" class="form-control" placeholder="Email" name="email" id="usernameLOG" textbox>
                            <span class="form-control-feedback"></span>
                            <span class="text-warning"></span>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" name="password" id="passwordLOG" textbox>
                            <span class="form-control-feedback"></span>
                            <span class="text-warning"></span>
                        </div>
                        <button type="submit" class="mb-3 btn btn-lg btn-primary btn-block" name="submit"><i class="fa fa-unlock"></i> Masuk</button>
                        <p class="mt-5 mb-3 text-muted text-center">&copy; <script>document.write(new Date().getFullYear());</script></p>
                    </form>
      </div>
    </div>
  </div>
</div>
<!-- Login Modal End -->

<!-- Register Modal -->
<div class="modal fade" id="daftar" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">REGISTRASI</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-signin" role="form" method="POST" action="?tampil=registrasi_proses">
            <div class="text-center mb-4">
                <p class="mb-3 font-weight-normal">Daftar Anggota Baru</p>
            </div>

            <div class="form-group has-feedback">
                <input id="nama" type="text" class="form-control" placeholder="Nama Lengkap" name="nama_konsumen" required textbox>
                <span class="form-control-feedback"></span>
                <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="nohp" type="number" class="form-control" placeholder="Nomor HP" name="phone_number" minlength="10" maxlength="12" required textbox>
                <span class="form-control-feedback"></span>
                <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
                <input id="email" type="email" name="email" class="form-control" placeholder="Email" required textbox>
                <span class="form-control-feedback"></span>
                <span class="text-warning"></span>
            </div>
            <div class="form-group has-feedback">
                <input class="form-control" id="password" name="password" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Minimal 6 Karakter' : ''); if(this.checkValidity()) form.password_two.pattern = this.value;" placeholder="Password" required>
                <span class="form-control-feedback"></span>
                <span class="text-warning"></span>
            </div>
             <div class="form-group has-feedback">
                <input class="form-control" id="password_two" name="password_two" type="password" pattern="^\S{6,}$" onchange="this.setCustomValidity(this.validity.patternMismatch ? 'Masukkan Password Yang Sama' : '');" placeholder="Verify Password" required>
                <span class="form-control-feedback"></span>
                <span class="text-warning"></span>
            </div>
            <button type="submit" name="submit" class="btn btn-md btn-primary btn-block mt-3"><i class="fa fa-send"></i> DAFTAR
            </button>
        <p class="mt-5 mb-3 text-muted text-center">&copy; <script>document.write(new Date().getFullYear());</script></p>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- Register Modal End -->
