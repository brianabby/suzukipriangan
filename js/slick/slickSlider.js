$('.slider-for').slick({
    slidesToShow: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true

});