<?php

    session_start();

    if (!empty ($_SESSION['email']) and ($_SESSION['password'])) {

    include ("lib/koneksi.php");
    define("INDEX", true);
        
    $sql = mysqli_query ($koneksi, "SELECT * FROM konsumen WHERE email='$_SESSION[email]'");
    $konsumen = mysqli_fetch_array ($sql);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>SUZUKI Priangan | PT Suzuki Indomobil</title>

    <!-- Favicon -->
    <link rel="icon" href="img/logo2.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
<?php include ("menu.php"); ?>
    </header>
<?php include ("konten.php"); ?>
    <footer class="footer-area section-padding-100-0">
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">PRODUK</h5>
                        <!-- Nav -->
                        <nav>
                            <ul>
                                <li><a href="https://suzukipriangan.com/?tampil=mobil">Mobil</a></li>
                                <li><a href="#">Aksesoris</a></li>
                                <li><a href="#">Sparepart</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">KOMUNITAS</h5>
                        <!-- Nav -->
                        <nav>
                            <ul>
                                <li><a href="#">Forum</a></li>
                                <li><a href="#">Bengkel</a></li>
                                <li><a href="https://suzukipriangan.com/?tampil=artikel">Artikel</a></li>
                                <li><a href="https://suzukipriangan.com/?tampil=tips_trik">Tips dan Trik</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">KONTAK KAMI</h5>
                        <!-- Single News Area -->
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="img/core-img/icon-large-02.png" alt="">
                            </div>
                            <div class="news-content">
                                <a href="#">Customer Service 1</a>
                                <div class="news-meta">
                                    <a href="#" class="post-author"> Indah</a>
                                    <a href="#" class="post-author"> +62 818-430-490</a>
                                </div>
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="img/core-img/icon-large-02.png" alt="">
                            </div>
                            <div class="news-content">
                                <a href="#">Customer Service 2</a>
                                <div class="news-meta">
                                    <a href="#" class="post-author"> Egi Jati</a>
                                    <a href="#" class="post-author"> +62 859-7492-3641</a>
                                </div>
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="img/core-img/icon-large-02.png" alt="">
                            </div>
                            <div class="news-content">
                                <a href="#">Customer Service 3</a>
                                <div class="news-meta">
                                    <a href="#" class="post-author"> Brian</a>
                                    <a href="#" class="post-author"> +62 853-3946-3945</a>
                                </div>
                            </div>
                        </div>
                        <!-- Single News Area -->
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-100">
                        <h5 class="widget-title">SISTEM PEMBAYARAN</h5>
                        <!-- Single News Area -->
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-visa.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-mastercard.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-jcb.png" alt="">
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-amex.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-alfa.png" alt="">
                            </div> &nbsp; &nbsp;
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-info.png" alt="">
                            </div>
                        </div>
                        <div class="single-latest-news-area d-flex align-items-center">
                            <div class="news-thumbnail">
                                <img src="https://moladin.com/images/footer-bank.png" alt="">
                            </div>
                        </div>
                        <!-- Single News Area -->
                    </div>
                </div>
            </div>
        </div>

        <!-- Copywrite Area -->
        <div class="copywrite-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copywrite-content d-flex flex-wrap justify-content-between align-items-center">
                            <!-- Footer Logo -->
                            <a href="index.php" class="footer-logo"></a>

                            <!-- Copywrite Text -->
                            <p class="copywrite-text"><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved By <a href="https://www.suzukipriangan.com" target="_blank">Suzuki Priangan</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/bootstrap/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <!-- All Plugins js -->
    <script src="js/plugins/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
    <!-- Slick js -->
    <script src="js/slick/slick.min.js"></script>
    <script src="js/slick/slickSlider.js"></script>
    <!-- Font-Awesome -->
    <script src="js/fa/all.min.js"></script>
    <!-- FB JS -->
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.3"></script>
</body>

</html>
<?php
        
    }else{
         header ("location: index.php");
    }

?>