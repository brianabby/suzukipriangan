
        <!-- Navbar Area -->
        <div class="credit-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="creditNav">

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="?tampil=beranda">BERANDA</a></li>
                                    <li><a href="#">GARASI</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=mobil">MOBIL</a></li>
                                            <!--<li><a href="?tampil=test-drive">TEST DRIVE</a></li>-->
                                            <!--<li><a href="#">SPAREPART</a></li>-->
                                            <li><a href="?tampil=aksesoris_kategori">AKSESORIS</a></li>
                                            <li><a href="?tampil=karoseri">KAROSERI</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">BENGKEL</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=service">BOOKING SERVICE</a></li>
                                            <!--<li><a href="#">PERAWATAN</a></li>-->
                                            <!--<li><a href="#">TEMUKAN BENGKEL</a></li>-->
                                        </ul>
                                    </li>
                                    <li><a href="#">CORNER</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=artikel">ARTIKEL</a></li>
                                            <li><a href="?tampil=tips_trik">TIPS & TRIK</a></li>
                                            <!--<li><a href="#">FORUM</a></li>-->
                                        </ul>
                                    </li>
                                    <li><a href="#">BAZAR</a>
                                        <ul class="dropdown">
                                            <li><a href="?tampil=promo">PROMO</a></li>
                                            <li><a href="?tampil=event">EVENT</a></li>
                                        </ul>
                                    </li>
                                    <?php
                                        if (defined ("INDEX")) {
                                    ?>
                                            <li><a href="?tampil=profil">Saya</a>
                                                <ul class="dropdown">
                                                    <li><a href="?tampil=profil">Profil</a></li>
                                                    <li><a href="?tampil=logout">Logout</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="?tampil=keranjang" class="mr-2" title="Keranjang Saya"><img src="http://suzukipriangan.com/img/core-img/shopping-cart.png" alt="" class="col-md-8 col-xs-8"></a></li>
                                    <?php
                                        }
                                    ?>
                                    
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>

                        <!-- Contact -->
                        <div class="contact">
                            <a href="https://suzukipriangan.com"><img src="img/logo3.png" alt=""></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>